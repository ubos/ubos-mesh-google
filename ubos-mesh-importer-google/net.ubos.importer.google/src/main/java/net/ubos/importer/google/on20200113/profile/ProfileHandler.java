//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.google.on20200113.profile;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.MalformedJsonException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.ubos.importer.handler.AbstractBasicFileImporterHandler;
import net.ubos.importer.handler.ImporterHandlerContext;
import net.ubos.importer.handler.ImporterHandlerNode;
import net.ubos.mesh.MeshObject;
import net.ubos.mesh.namespace.MeshObjectIdentifierNamespace;
import net.ubos.meshbase.MeshBase;
import net.ubos.model.Google.GoogleSubjectArea;
import net.ubos.model.Identity.IdentitySubjectArea;
import net.ubos.model.primitives.EnumeratedValue;
import net.ubos.model.primitives.StringValue;
import net.ubos.model.primitives.TimeStampValue;

/**
 *
 */
public class ProfileHandler
    extends
        AbstractBasicFileImporterHandler
{
    /**
     * Constructor.
     *
     * @param okScore the score
     * @param filenamePattern the path plus filename pattern this ImporterContentHandler can handle.
     */
    public ProfileHandler(
            String filenamePattern )
    {
        super( Pattern.compile( filenamePattern ), PERFECT );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double doImport(
            ImporterHandlerNode    toBeImported,
            ImporterHandlerContext context )
        throws
            ParseException,
            IOException
    {
        if( !toBeImported.canGetStream() ) {
            return IMPOSSIBLE;
        }

        double ret;

        try {
            InputStream in = toBeImported.createStream();

            ret = jsonReaderImport( new JsonReader( new InputStreamReader( in )), context );

        } catch( MalformedJsonException ex ) {
            ret = IMPOSSIBLE;
        }
        return ret;
    }

    /**
     * Knows how to import JSON from a JsonReader.
     *
     * @param jr the to-be imported stream
     * @param context context for the importing
     * @return the score
     * @throws ParseException a parsing error occurred
     * @throws IOException an I/O project occurred
     */
    protected double jsonReaderImport(
            JsonReader           jr,
            ImporterHandlerContext context )
        throws
            ParseException,
            IOException
    {
        double ret;
        switch( jr.peek() ) {
            case BEGIN_OBJECT:
                ret = jsonReadProfile( jr, context );
                break;

            default:
                ret = IMPOSSIBLE;
                break;
        }
        return ret;
    }

    protected double jsonReadProfile(
            JsonReader           jr,
            ImporterHandlerContext context )
        throws
            ParseException,
            IOException
    {
        MeshBase mb = context.getMeshBase();

        MeshObject myAccount = mb.getHomeObject();
        myAccount.bless( GoogleSubjectArea.GOOGLEACCOUNT );

        MeshObject myAccountProfile = mb.createMeshObject(
                "profile",
                GoogleSubjectArea.GOOGLEACCOUNTPROFILE );

        myAccountProfile.blessRole( IdentitySubjectArea.ACCOUNTPROFILE_FOR_ACCOUNT_S, myAccount );

        jr.beginObject();
        while( jr.hasNext() ) {
            String name = jr.nextName();
            String value;

            switch( name ) {
                case "name":
                    jsonReadProfileName( jr, myAccountProfile, mb );
                    break;

                case "gender":
                    jsonReadProfileGender( jr, myAccountProfile, mb );
                    break;

                case "birthday":
                    value = jr.nextString();
                    myAccountProfile.setPropertyValue( IdentitySubjectArea.INDIVIDUALACCOUNTPROFILE_DATEOFBIRTH, parseDate( value ));

                default:
                    jr.skipValue();
                    break;
            }
        }
        jr.endObject();

        StringValue fullName = (StringValue) myAccountProfile.getPropertyValue( IdentitySubjectArea.INDIVIDUALACCOUNTPROFILE_FULLNAME );
        if( fullName != null ) {
            MeshObjectIdentifierNamespace thisNs = mb.getDefaultNamespace();
            thisNs.addExternalName(          USER_NAMESPACE_PREFIX + fullName.getValue() );
            thisNs.setPreferredExternalName( USER_NAMESPACE_PREFIX + fullName.getValue() );
        }

        return theOkScore;
    }

    protected double jsonReadProfileName(
            JsonReader jr,
            MeshObject myAccountProfile,
            MeshBase   mb )
        throws
            ParseException,
            IOException
    {
        jr.beginObject();
        while( jr.hasNext() ) {
            String name = jr.nextName();
            String value;

            switch( name ) {
                case "formattedName":
                    value = jr.nextString();
                    myAccountProfile.setPropertyValue( IdentitySubjectArea.INDIVIDUALACCOUNTPROFILE_FULLNAME, StringValue.create( value ));
                    break;

                case "givenName":
                    value = jr.nextString();
                    myAccountProfile.setPropertyValue( IdentitySubjectArea.INDIVIDUALACCOUNTPROFILE_FIRSTNAME, StringValue.create( value ));
                    break;

                case "familyName":
                    value = jr.nextString();
                    myAccountProfile.setPropertyValue( IdentitySubjectArea.INDIVIDUALACCOUNTPROFILE_LASTNAME, StringValue.create( value ));
                    break;

                default:
                    jr.skipValue();
                    break;
            }
        }
        return theOkScore;
    }

    protected double jsonReadProfileGender(
            JsonReader jr,
            MeshObject myAccountProfile,
            MeshBase   mb )
        throws
            ParseException,
            IOException
    {
        jr.beginObject();
        while( jr.hasNext() ) {
            String name = jr.nextName();
            String value;

            switch( name ) {
                case "type":
                    value = jr.nextString();
                    myAccountProfile.setPropertyValue( IdentitySubjectArea.INDIVIDUALACCOUNTPROFILE_GENDER, theGenderTable.get( value ));
                    break;

                default:
                    jr.skipValue();
                    break;
            }
        }
        return theOkScore;
    }

    /**
     * How to parse a date given as YYYY-MM-NN
     * @param s
     * @return
     */
    protected TimeStampValue parseDate(
            String s )
    {
        final Pattern  dayPattern = Pattern.compile( "(\\d\\d\\d\\d)-(\\d\\d)-(\\d\\d)" );
        TimeStampValue ret;

        Matcher m = dayPattern.matcher( s );
        if( m.matches() ) {
            ret = TimeStampValue.create(
                    Integer.parseInt( m.group( 1 )),
                    Integer.parseInt( m.group( 2 )) + 1,
                    Integer.parseInt( m.group( 3 )),
                    0, 0, 0 );
        } else {
            ret = null;
        }
        return ret;
    }

    /**
     * The prefix for all MeshObjectIdentifierNamespaces that we allocate for users.
     */
    public static final String USER_NAMESPACE_PREFIX = "google.com/user/";

    /**
     * Translate the Google gender into our gender.
     */
    public static final Map<String,EnumeratedValue> theGenderTable = Map.of(
            "male",   IdentitySubjectArea.INDIVIDUALACCOUNTPROFILE_GENDER_type_MALE,
            "female", IdentitySubjectArea.INDIVIDUALACCOUNTPROFILE_GENDER_type_FEMALE );
}
