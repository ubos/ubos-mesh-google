//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.google;

import net.ubos.importer.google.on20200113.GoogleImporter20200113;
import net.ubos.importer.Importer;
import org.diet4j.core.Module;
import org.diet4j.core.ModuleActivationException;

/**
 * Activate this Module and return an Importer
 */
public class ModuleInit
{
    /**
     * Diet4j module activation.
     *
     * @param thisModule the Module being activated
     * @return the Importer instances
     * @throws ModuleActivationException thrown if module activation failed
     */
    public static Importer [] moduleActivate(
            Module thisModule )
        throws
            ModuleActivationException
    {
        return new Importer[] {
            new GoogleImporter20200113()
        };
    }
}
