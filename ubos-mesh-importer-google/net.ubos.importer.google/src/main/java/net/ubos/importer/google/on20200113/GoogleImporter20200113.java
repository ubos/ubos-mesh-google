//
// Copyright (C) Dazzle Labs, Inc. All rights reserved. License: see package.
//

package net.ubos.importer.google.on20200113;

import java.io.IOException;
import net.ubos.importer.google.on20200113.profile.ProfileHandler;
import net.ubos.importer.handler.AbstractMultiHandlerImporter;
import net.ubos.importer.handler.ImporterHandler;
import net.ubos.importer.handler.csv.DefaultCsvImporterHandler;
import net.ubos.importer.handler.directory.SkipIntermediateDirectoryImporterHandler;
import net.ubos.importer.handler.json.DefaultJsonImporterHandler;
import net.ubos.importer.handler.remainders.RateRemaindersImporterHandler;
import net.ubos.importer.handler.vcf.DefaultVcfImporterHandler;
import net.ubos.importer.handler.zip.DefaultZipImporterHandler;
import net.ubos.importer.handler.zip.NoOpZipDirectoryImporterHandler;
import net.ubos.util.logging.Log;

/**
 * Imports a Google data export ZIP file.
 */
public class GoogleImporter20200113
    extends
        AbstractMultiHandlerImporter
{
    private static final Log log = Log.getLogInstance( GoogleImporter20200113.class );

    /**
     * Constructor.
     */
    public GoogleImporter20200113()
    {
        super( "Google importer (format as of 2020-01-13)", HANDLERS, RATE_REMAINDERS_HANDLER );
    }

    /**
     * Name of the file that contains the expected entry patterns.
     */
    public static final String EXPECTED_ENTRY_PATTERNS_FILE = "ExpectedEntryPatterns.txt";

    /**
     * The ImporterContentHandlers for this Importer
     */
    public static final ImporterHandler [] HANDLERS = {
        // Structure first
            new SkipIntermediateDirectoryImporterHandler(), // support unpacked directories, too
            new DefaultZipImporterHandler(),

        // Profile
            new ProfileHandler(
                    "[^/]+/Takeout/Profile/Profile\\.json" ),

        // Contacts/All Contacts
            new DefaultVcfImporterHandler(
                    "[^/]+/Takeout/Contacts/All Contacts/All Contacts\\.vcf", ImporterHandler.PERFECT ),

        // Fallback
            new NoOpZipDirectoryImporterHandler( ImporterHandler.UNRATED, true ),
            new DefaultJsonImporterHandler( ImporterHandler.FALLBACK ),
            new DefaultCsvImporterHandler( ImporterHandler.FALLBACK ),
    };

    public static final RateRemaindersImporterHandler RATE_REMAINDERS_HANDLER;
    static {
        RateRemaindersImporterHandler handler = null;
        try {
            handler = new RateRemaindersImporterHandler(
                        GoogleImporter20200113.class.getResourceAsStream( EXPECTED_ENTRY_PATTERNS_FILE ),
                        ImporterHandler.SUFFICIENT, // we aren't actually parsing, but that's intentionally so
                        ImporterHandler.MAYBE_WRONG );

        } catch( IOException ex ) {
            log.error( ex );
        }
        RATE_REMAINDERS_HANDLER = handler;
    }
}
